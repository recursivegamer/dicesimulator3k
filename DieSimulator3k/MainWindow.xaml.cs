﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DieSimulator3k
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        int attackingUnits = 0;
        int defendingUnits = 0;
        int attackingDiceType = 0;
        int attackDieNumber = 1;
        int defendingDiceType = 0;
        int defenceDieNumber = 1;
        int attackerWins = 0;
        int defenderWins = 0;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(sender is TextBox)
            {
                TextBox tb = (TextBox)sender;
                String input = tb.Text;
                int numberResult;
                if (int.TryParse(input, out numberResult))
                {
                    if (tb.Equals(AttackUnits))
                    {
                        attackingUnits = numberResult;
                    }
                    else if (tb.Equals(DefenceUnits))
                    {
                        defendingUnits = numberResult;
                    }
                    else if (tb.Equals(AttackDice))
                    {
                        attackingDiceType = numberResult;
                    }
                    else if (tb.Equals(DefenceDice))
                    {
                        defendingDiceType = numberResult;
                    }
                    else if (tb.Equals(AttackNumberDie))
                    {
                        attackDieNumber = numberResult;
                    }
                    else if (tb.Equals(DefenceNumberDie))
                    {
                        defenceDieNumber = numberResult;
                    }
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(sender.Equals(Fight) && defendingDiceType > 0 && attackingDiceType > 0 && defendingUnits > 0 && attackingUnits > 0)
            {
                defenderWins = 0;
                attackerWins = 0;
                DefenceWins.Text = defenderWins.ToString();
                AttackWins.Text = attackerWins.ToString();
                for (int i = 0; i < 1000000; i++)
                {
                    int aUnits = attackingUnits;
                    int dUnits = defendingUnits;
                    while (dUnits > 0 && aUnits > 0)
                    {
                        Random rand = new Random();
                        List<int> attacks = new List<int>();
                        List<int> defence = new List<int>();
                        for(int j = 0; j < aUnits; j++)
                        {
                            int attackTotal = 0;
                            for (int h = 0; h < attackDieNumber; h++)
                            {
                                attackTotal += rand.Next(1, attackingDiceType + 1);
                            }
                            attacks.Add(attackTotal);
                        }  
                        for (int j = 0; j < dUnits; j++)
                        {
                            int defenceTotal = 0;
                            for (int h = 0; h < defenceDieNumber; h++)
                            {
                                defenceTotal += rand.Next(1, defendingDiceType + 1);
                            }
                            defence.Add(defenceTotal);
                        }
                        attacks.Sort((a, b) => -1 * a.CompareTo(b));
                        defence.Sort((a, b) => -1 * a.CompareTo(b));
                        int attackWins = 0;
                        int defenceWins = 0;
                        if(attacks[0] > defence[0])
                        {
                            attackWins++;
                            for(int j = 1; j < attacks.Count; j++)
                            {
                                if(attacks[j] < defence[0])
                                {
                                    attackWins++;
                                } else
                                {
                                    break;
                                }
                            }
                        } else if(attacks[0] < defence[0])
                        {
                            defenceWins++;
                            for (int j = 1; j < defence.Count; j++)
                            {
                                if (defence[j] < attacks[0])
                                {
                                    defenceWins++;
                                } else
                                {
                                    break;
                                }
                            }
                        }
                        aUnits = aUnits - defenceWins;
                        dUnits = dUnits - attackWins;
                    }
                    if(aUnits <= 0)
                    {
                        defenderWins++;
                    } else if (dUnits <= 0)
                    {
                        attackerWins++;
                    }                    
                }
            }
            DefenceWins.Text = defenderWins.ToString();
            AttackWins.Text = attackerWins.ToString();
        }
    }
}
